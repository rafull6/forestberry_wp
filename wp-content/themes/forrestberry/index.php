<?php get_header(); ?>
<div class="bg-1">
  <div class="container_16">
      <div class="row row-1">
        <?php
           $args = array('cat' => 6);
           $content_section_1_posts = new WP_Query($args);
           if($content_section_1_posts->have_posts()){
              while($content_section_1_posts->have_posts()){
                 $content_section_1_posts->the_post();
                 get_template_part('content-section-1');
               }
             }
          ?>
      </div>
  </div>
        </div>
        <div class="container_16">
            <div class="row">
              <?php
                 $args = array('cat' => 7);
                 $content_section_2_posts = new WP_Query($args);
                 if($content_section_2_posts->have_posts()){
                    while($content_section_2_posts->have_posts()){
                       $content_section_2_posts->the_post();
                       get_template_part('content-section-2');
                     }
                   }
                ?>
                <div class="grid_4">
                    <h2>new services</h2>
                    <div class="num-lists">
                      <?php
                         $args = array('cat' => 8);
                         $content_section_2_posts = new WP_Query($args);
                         if($content_section_2_posts->have_posts()){
                            while($content_section_2_posts->have_posts()){
                               $content_section_2_posts->the_post();
                               get_template_part('content-section-3');
                             }
                           }
                        ?>
                    </div>
    				<a href="#" class="btn-2 inverted">more news</a>
                </div>
            </div>
        </div>
<?php get_footer(); ?>
