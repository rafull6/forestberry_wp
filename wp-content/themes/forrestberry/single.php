<?php get_header(); ?> 
<div class="bg-1">
	<a name="postview">
	<div class="post-container">
		<div class="post-title">
			<?php the_title(); ?>
		</div>
		<div class="post-content">
			<div class="container"><?php the_content(); ?></div>
		</div>
		<div class="post-info">
			<span>Дата: </span><?php the_date(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>