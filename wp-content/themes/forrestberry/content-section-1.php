<div class="grid_4 prefix_1 bord-1 btn-wrapper">
    <div class="maxheight">
        <div class="aligncenter">
        <?php  if ( has_post_thumbnail() ) {
            the_post_thumbnail();
          } ?>
        </div>
        <h2><?php the_title(); ?></h2>
        <div class="separator">
            <img src="<?php bloginfo('template_directory'); ?>/images/separator.png" alt="" />
        </div>
        <div class="preview-content">
            <p>
              <?php
                $content = get_the_content();
                echo wp_trim_words( $content , '25' ); ?>
            </p>
        </div>
        <a class="btn" href="<?php echo esc_url( get_permalink() ); ?>#postview"><span>more</span></a>

    </div>
</div>
