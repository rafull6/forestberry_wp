<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>

	<script>
					jQuery(window).load(function($){
	            jQuery('#camera_wrap_1').camera({
	                height: '73,00%',
	                // thumbnails: true,
	                playPause: false,
	                time: 8000,
	                transPeriod: 900,
	                fx: 'simpleFade',
	                loader: 'none',
	                minHeight: '200px',
	                navigation: false
	            });
							jQuery('#foo').carouFredSel({
	                auto: false,
	                responsive: true,
	                width: '100%',
	                prev: '#prev1',
	                next: '#next1',
	                scroll: 1,
	                items: {
	                    height: 'auto',
	                    width: 300,
	                    visible: {
	                        min: 1,
	                        max: 1
	                    }
	                },
	                mousewheel: false,
	                swipe: {
	                    onMouse: true,
	                    onTouch: true
	                }
	            });
	        });
	    </script>
</head>

<body <?php body_class(); ?>>
	    <header>
	        <div class="container_16">
	            <div class="row">
	                <div class="grid_16">
	                    <h1><a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="Колпинка"></a> </h1>
	                    <nav>
												<?php wp_nav_menu( array( 'theme_location' => '', 'menu_class' => 'sf-menu') ); ?>
	                    </nav>
	                </div>
	            </div>
	        </div>
	        <div class="bg-1">
	            <div class="bg-2">
	                <div class="container_16">
	                    <div class="row">
	                        <div class="grid_16 overflowa clearfix">
														<div class="headerslider">
														 <?php echo do_shortcode('[sp_responsiveslider cat_id="4" design="design-3" effect="fade" navigation="false" speed="3000" height="439"]'); ?>
														 </div>
	                        </div>
	                    </div>
	                    <div class="shadow"></div>
	                    <div class="row">
	                        <div class="grid_16">
	                            <div class="title-1">Sed ut perspiciatis unde omnis iste natus</div>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class="list_carousel responsive clearfix carousel-1">
	                            <ul id="foo" class="clearfix">
	                            <?php
		                         $args = array('cat' => 5);
		                         $content_section_2_posts = new WP_Query($args);
		                         if($content_section_2_posts->have_posts()){
		                            while($content_section_2_posts->have_posts()) : $content_section_2_posts->the_post(); ?>
		                          <li class="grid_16"><div class="text-1">
										<?php the_content(); ?>
										</div>
									</li>
									<?php
									endwhile;
								}
									wp_reset_postdata();
									?>
	                            </ul>
	                            <div class="arrows">
	                                <a id="prev1" class="prev" href="#"></a>
	                                <a id="next1" class="next" href="#"></a>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </header>
	    <div class="content">
