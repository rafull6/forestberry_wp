<?php get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
		while ( have_posts() ) : the_post(); ?>
		<article>
			<div class="bg-1">
				<a name="entryview"></a>
				<div class="entry-container">
					<div class="entry-title">
						<?php the_title(); ?>
						<div class="entry-edit">
							<?php edit_post_link('Изменить'); ?>
						</div>
					</div>
					<div class="entry-content">
						<div class="container"><?php the_content(); ?></div>
					</div>
					<div class="entry-info">
						<span>Дата: </span><?php the_date(); ?>
					</div>
				</div>
			</div>
		</article>
		<?php endwhile; ?>
		?>
	</main><!-- .site-main -->
</div><!-- .content-area -->
<?php get_footer(); ?>
