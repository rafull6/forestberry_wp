</div>
    <footer>
        <div class="bg-3">
            <div class="container_16 bord-3">
                <div class="row main-foot">
                    <div class="container_16">
                        <div class="grid_4 bord-1">
                          <!-- socials -->
                          <ul class="list-services clearfix">
                           </ul>
                        </div>
                        <div class="grid_6 bord-1 prefix_1">
                          <!-- Adress -->
                            <?php
                              $post_id = 92;
                              $queried_post = get_post($post_id);
                            ?>
                          <div class="icon-1">
                            <?php echo $queried_post->post_content; ?>
                          </div>
                          <?php wp_reset_postdata() ?>
                        </div>
                        <div class="grid_4 prefix_1">
                            <?php
                              $post_id = 107;
                              $queried_post = get_post($post_id);
                            ?>
                            <?php $adress = new WP_Query("cat=9&showposts=107");?>
                              <div class="icon-2"><a href="mailto:<?php echo $queried_post->post_content; ?>" class="color-2 h-underline"><?php echo $queried_post->post_content; ?></a></div>
                            <?php wp_reset_postdata();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-4">
            <div class="container_16">
                <div class="row grid_16">
                    <div class="inside">
                        <a href="index.html">
                          <img src="<?php bloginfo('template_directory'); ?>/images/footer-logo.png" alt=""></a>
                        <br/>
                        <span>&copy; 2016 &#8226; <a href="№" class="h-underline">Политика конфиденциальности</a></span>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php wp_footer(); ?>
</body>
</html>
