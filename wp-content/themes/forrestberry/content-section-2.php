<div class="grid_5 suffix_1">
    <div class="wrapper p1">
      <?php  if ( has_post_thumbnail() ) {
      the_post_thumbnail();
    } ?>
  </div>
    <div class="title-2"><?php the_title(); ?></div>
    <div>
      <?php the_content(); ?>
    </div>
    <a href="<?php echo esc_url( get_permalink() ); ?>#postview" class="btn-2">read more</a>
</div>
